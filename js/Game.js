// 分别是 画笔 鸟类 管子类 地面(背景实例 ) 山（背景实力）
function Game(ctx ,bird,pipe,land,mountain){
	this.ctx = ctx;
	this.bird =  bird ;
	this.pipeArr = [pipe];
	this.land = land;
	this.mountain = mountain;
	this.timer = null;
	// 定义帧用与控制翅膀煽动速度
	this.iframe = 0;

	this.inite();
}

Game.prototype.inite=function(){
	this.star()
	this.bindEvent()
}
// 渲染背景
Game.prototype.renderMountain =function(){
	var img =this.mountain.img
	this.mountain.x -= this.mountain.step
	// // 边界判定
	if(this.mountain.x < -img.width){
		this.mountain.x = 0 
	}
	this.ctx.drawImage(img,this.mountain.x,this.mountain.y);
	this.ctx.drawImage(img,img.width+this.mountain.x,this.mountain.y)
	this.ctx.drawImage(img,img.width*2+this.mountain.x,this.mountain.y)
}
Game.prototype.renderLand = function(){
	var img = this.land.img;
	this.land.x -= this.land.step
	// 边界判断
	if(this.land.x < -img.width){
		this.land.x = 0
	}
	this.ctx.drawImage(img,this.land.x,this.land.y)
	this.ctx.drawImage(img,img.width+this.land.x,this.land.y)
	this.ctx.drawImage(img,img.width*2+this.land.x,this.land.y)
}
// 渲染鸟
Game.prototype.renderBird =function(){
	// 获取鸟的图片
	var img =this.bird.img;
	// 保存状态
	this.ctx.save();
	// 平移坐标
	this.ctx.translate(this.bird.x, this.bird.y)
	// 描边矩形
	// this.ctx.strokeRect(-img.width/2+6,-img.height/2 + 8,this.bird.img.width-15,this.bird.img.height-15)
	//判断鸟的装态
	var deg = this.bird.state ==="D"?Math.PI / 180 *this.bird.speed : Math.PI / 180 *-this.bird.speed
	// 旋转
	this.ctx.rotate(deg)
	// 渲染鸟
	this.ctx.drawImage(img,-img.width/2,-img.height/2)
	// 恢复状态
	this.ctx.restore()

}
// 开始游戏
Game.prototype.star = function(){
	var me = this;
	this.timer=setInterval(function(){
		me.iframe++;
		// 清平
		me.clear()
		// 、渲染
		me.renderMountain()
		// 渲染地面
		me.renderLand()
		// 渲染鸟
		me.renderBird()
		// 飞翔fly
		if(!(me.iframe % 10)){
			me.bird.fly()
			// console.log(me.iframe)
		}
		// 鸟下降
		me.bird.falwDown()
		// 管子移动
		me.movePipe()
		// 渲染管子
		me.renderPipe()
		// 创建管子
		if(!(me.iframe % 65)){
			me.cratePipe()
		}
		// 清除数组
		me.clearPipe()
		// 渲染方块
		me.renderPoints()
		me.renderPipePoints()
		me.changBoom()
	},20)
}
// 清平方法
Game.prototype.clear = function(){
	this.ctx.clearRect(0,0,1000,1000)
}
// 点击事件
Game.prototype.bindEvent=function(){
	var me = this;
	this.ctx.canvas.onclick = function(){
		me.bird.goUp()
	}
}
// 渲染管子
Game.prototype.renderPipe = function(){
	// 保存this
	var me = this;
	// 循环渲染管子
	this.pipeArr.forEach(function(value,index){
		var img_up = value.pipe_up;
		// 图片的x值
		var img_x = 0 ;
		// 图片的y值
		var img_y =  img_up.height -  value.up_height;
		// 上馆子图片的宽
		var img_w = img_up.width;
		// 上馆子图片的高
		var img_h = value.up_height;
		// canvas上的x点
		var canvas_x = me.ctx.canvas.width -(value.step* value.count);
		// canvas上的y点
		var canvas_y = 0;
		// canvas上的宽
		var canvas_w = img_w;
		// canvas上的高
		var canvas_h = img_h;
		// 渲染管子
		me.ctx.drawImage(img_up,img_x,img_y,img_w,img_h,canvas_x,canvas_y,canvas_w,canvas_h)
		// 渲染下管子
		var img_down = value.pipe_down;
		// 图片的x值
		var img_down_x = 0 ;
		// 图片的y值
		var img_down_y =  0;
		// 上馆子图片的宽
		var img_down_w = img_down.width;
		// 上馆子图片的高
		var img_down_h = value.down_height;
		// canvas上的x点
		var canvas_down_x = me.ctx.canvas.width -(value.step* value.count);
		// canvas上的y点
		var canvas_down_y = img_h +150;
		// canvas上的宽
		var canvas_down_w = img_down_w;
		// canvas上的高
		var canvas_down_h = img_down_h;
		// 渲染管子
		me.ctx.drawImage(img_down,img_down_x,img_down_y,img_down_w,img_down_h,canvas_down_x,canvas_down_y,canvas_down_w,canvas_down_h)
	})
}
// 管子移动
Game.prototype.movePipe =function(){
	this.pipeArr.forEach(function(value,index){
		value.count++;
	})
}
// 创建管子
Game.prototype.cratePipe = function(){
	var pipe = this.pipeArr[0].createPipe();
	this.pipeArr.push(pipe);
	// console.log(pipeArr)
}
// 清除管子方法
Game.prototype.clearPipe=function(){
	for(var i = 0 ;i <this.pipeArr.length ;i++){
		var pipe =this.pipeArr[i]
		if(pipe.x-pipe.step*pipe.count < -pipe.pipe_up.width){
			this.pipeArr.splice(i,1)
		}

	}
}
// 绘制鸟的4个点
Game.prototype.renderPoints=function(){
	var bird_A ={
		x:-this.bird.img.width/2+10+this.bird.x,
		y:-this.bird.img.height/2 + 15 + this.bird.y
	}
	var bird_B ={
		x:bird_A.x +this.bird.img.width-24,
		y:bird_A.y
	}
	var bird_C={
		x:bird_A.x,
		y:bird_A.y+this.bird.img.height-24
	}
	var bird_D = {
		x:bird_A.x +this.bird.img.width-24,
		y:bird_A.y+this.bird.img.height-24
	}
	this.ctx.beginPath();
	// 位移画笔到某个位置
	this.ctx.moveTo(bird_A.x,bird_A.y);
	this.ctx.lineTo(bird_B.x,bird_B.y);
	this.ctx.lineTo(bird_D.x,bird_D.y);
	this.ctx.lineTo(bird_C.x,bird_C.y);
	// this.ctx.stroke();
	this.ctx.closePath();
}

Game.prototype.renderPipePoints = function(){
	for(var i = 0 ;i< this.pipeArr.length ; i++){
		// 绘制一根管子
		var pipe = this.pipeArr[i];
		// 绘制管子上的4 个点
		var pipe_A = {
			x: this.ctx.canvas.width -(pipe.step* pipe.count),
			y :  0 
		}
		var pipe_B = {
			x : pipe_A.x+ pipe.pipe_up.width,
			y : 0
		}
		var pipe_C = {
			x : pipe_A.x,
			y : pipe.up_height
		}
		var pipe_D = {
			x : pipe_B.x ,
			y : pipe.up_height
		}
		// 描边上馆子
		this.ctx.beginPath();
		this.ctx.moveTo(pipe_A.x, pipe_A.y);
		this.ctx.lineTo(pipe_B.x, pipe_B.y);
		this.ctx.lineTo(pipe_D.x, pipe_D.y);
		this.ctx.lineTo(pipe_C.x, pipe_C.y);
		this.ctx.closePath();
		this.ctx.strokeStyle = "red";
		// this.ctx.stroke();
		var pipe_down_A= {
			x : pipe_A.x ,
			y : pipe.up_height + 150
		}
		var pipe_down_B= {
			x : pipe_B.x ,
			y : pipe_down_A.y
		}
		var pipe_down_C= {
			x :  pipe_A.x ,
			y : 400
		}
		var pipe_down_D= {
			x :  pipe_B.x ,
			y : 400
		}
		this.ctx.beginPath();
		this.ctx.moveTo(pipe_down_A.x, pipe_down_A.y);
		this.ctx.lineTo(pipe_down_B.x, pipe_down_B.y);
		this.ctx.lineTo(pipe_down_D.x, pipe_down_D.y);
		this.ctx.lineTo(pipe_down_C.x, pipe_down_C.y);
		this.ctx.closePath();
		// this.ctx.strokeStyle = "white";
		// this.ctx.stroke();
	}
}
// 能力检测
Game.prototype.changBoom= function(){
	for(var i = 0 ;i< this.pipeArr.length ; i++){
		// 绘制一根管子
		var pipe = this.pipeArr[i];
		// 绘制管子上的4 个点
		var pipe_A = {
			x: this.ctx.canvas.width -(pipe.step* pipe.count),
			y :  0 
		}
		var pipe_B = {
			x : pipe_A.x+ pipe.pipe_up.width,
			y : 0
		}
		var pipe_C = {
			x : pipe_A.x,
			y : pipe.up_height
		}
		var pipe_D = {
			x : pipe_B.x ,
			y : pipe.up_height
		}
		
		var pipe_down_A= {
			x : pipe_A.x ,
			y : pipe.up_height + 150
		}
		var pipe_down_B= {
			x : pipe_B.x ,
			y : pipe_down_A.y
		}
		var pipe_down_C= {
			x :  pipe_A.x ,
			y : 400
		}
		var pipe_down_D= {
			x :  pipe_B.x ,
			y : 400
		}
		
	// 绘制鸟的4个点
		var bird_A ={
			x:-this.bird.img.width/2+6+this.bird.x,
			y:-this.bird.img.height/2 + 8 + this.bird.y
		}
		var bird_B ={
			x:bird_A.x +this.bird.img.width-15,
			y:bird_A.y
		}
		var bird_C={
			x:bird_A.x,
			y:bird_A.y+this.bird.img.height-15
		}
		var bird_D = {
			x:bird_A.x +this.bird.img.width-15,
			y:bird_A.y+this.bird.img.height-15
		}
		// 能力检测
		if(bird_B.x >= pipe_C.x && bird_B.y <= pipe_C.y && bird_A.x <= pipe_B.x ){
			console.log("撞到了")
			this.over();
			return;
		}
		if(bird_D.x >= pipe_down_A.x&& bird_D.y>= pipe_down_A.y&&bird_C.x <= pipe_down_B.x){
		this.over();
		return ;
		}
	}
}
Game.prototype.over = function(){
	console.log(123)
	clearInterval(this.timer)
}
