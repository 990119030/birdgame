function Bird(imgArr,x,y){
	// 3张图片所以用数组
	this.imgArr = imgArr;
	// 平移鸟的x，y值
	this.x =x ;
	this.y = y;
	// 定义图片索引
	this.index = parseInt(Math.random()* this.imgArr.length);
	this.img = this.imgArr[this.index];
	// 定义变量用于保存状态
	this.state = "D";
	// 定义信号量该表下降上生的速度
	this.speed = 0 ;
}
// 上动翅膀
Bird.prototype.fly=function(){
	// 改变索引
	this.index++;
	// 边界限定
	if(this.index > this.imgArr.length-1){
		this.index = 0 ;
	}
	this.img = this.imgArr[this.index]
}
// 鸟下降
Bird.prototype.falwDown = function(){
	if(this.state ==="D"){
		this.speed++;
		this.y += Math.sqrt(this.speed);
	}else{
		this.speed--;
		if(this.speed === 0){
			this.state = "D";
			return;
		}
		this.y -= Math.sqrt(this.speed);
	}
}
// 点击鸟上升
Bird.prototype.goUp =function(){
	this.speed = 20;
	this.state = "U"
}